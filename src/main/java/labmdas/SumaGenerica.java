package labmdas;

@FunctionalInterface
public interface SumaGenerica {

	int suma(int values[]);
	
}
