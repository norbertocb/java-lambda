package labmdas;

public class Lambda implements PorDefecto{

	public static void main(String[] args) {
		//() -> "Mi nombre es";
		
		MiNombre miNombreAnonima = new MiNombre() {
			@Override
			public String miNombre() {
				return "Norber Imperativo";
			}
		};
		
		System.out.println(miNombreAnonima.miNombre());
		
		MiNombre miNombreLambda = () -> "Norbert Lambda";
		System.out.println(miNombreLambda.miNombre());
		
		
		Sumar suma = new Sumar() {
			
			@Override
			public int suma(int a, int b) {
				return a + b;
			}
		};
		
		System.out.println(suma.suma(3, 7));
		
		Sumar sumaLambda = (a,b) -> a + b;
		System.out.println(sumaLambda.suma(6, 9));
		
		Sumar sumaLambda2 = (a,b) -> {
			a= b * b;
			a= a + b;
			System.out.println("Mensaje dentro de lambda");
			return a;
		};
		
		System.out.println(sumaLambda2.suma(2, 3));
		
		Lambda l = new Lambda();
		
		System.out.println(l.nombrePorDefecto("Norbert"));
		
		int arraySum[] = {1,2};
		
//		SumaGenerica sumaGen = {a,b} -> {
//			for (int i=0; i<= arraySum.length; i++ ) {
//				System.out.println(arraySum[i]);
//			}
//			return 0;
//		};
//		
//		sumaGen.suma(arraySum);
	}

	@Override
	public void mostrarNombre(String nombre) {
		// TODO Auto-generated method stub
		
	}
	

}
